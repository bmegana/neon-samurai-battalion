﻿using UnityEngine;

[CreateAssetMenu(
    fileName = "SoundFXClips",
    menuName = "ScriptableObjects/SoundFXClips",
    order = 2
)]
public class SoundFXClips : ScriptableObject
{
    public AudioClip slash;
    public AudioClip block;
}
