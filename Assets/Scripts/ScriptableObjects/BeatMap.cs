﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    fileName = "BeatMap",
    menuName = "ScriptableObjects/BeatMap",
    order = 1
)]
[ExecuteAlways]
public class BeatMap : ScriptableObject
{
    [SerializeField] private AudioClip song = null;
    [SerializeField] private int beatsPerMinute = 0;
    [SerializeField] private float offset = 0;

    private enum TimeSignature
    {
        FourByFour,
        ThreeByFour,
        FiveByFour,
        SevenByFour
    }
    [SerializeField] private TimeSignature timeSignature =
        TimeSignature.FourByFour;

    private int numMeasures;
    [System.Serializable]
    private class Measure
    {
        public float[] notePositions = null;
    }
    [SerializeField] private Measure[] measures = null;

    private List<float> beatPositions = null;

    public AudioClip GetSong()
    {
        return song;
    }

    public float GetOffset()
    {
        return offset;
    }

    public List<float> GetBeatPositions()
    {
        return beatPositions;
    }

    private int GetBeatMultiple()
    {
        switch (timeSignature)
        {
            case TimeSignature.FourByFour:
                return 4;
            case TimeSignature.ThreeByFour:
                return 3;
            case TimeSignature.FiveByFour:
                return 5;
            case TimeSignature.SevenByFour:
                return 7;
            default:
                return 4;
        }
    }

    private void OnEnable()
    {
        numMeasures = Mathf.FloorToInt(
            (song.length - offset) / (60.0f / beatsPerMinute) / 4.0f
        );
        if (measures.Length != numMeasures)
        {
            measures = new Measure[numMeasures];
        }

        int beatMultiple = GetBeatMultiple();
        float beatsPerSecond = beatsPerMinute / 60.0f;
        beatPositions = new List<float>();
        for (int i = 0; i < measures.Length; i++)
        {
            Measure m = measures[i];
            for (int j = 0; j < m.notePositions.Length; j++)
            {
                float beatPos = i * beatMultiple + m.notePositions[j] + 1.0f;
                float beatPosInSec = (1 / beatsPerSecond) * beatPos;
                beatPositions.Add(beatPosInSec - offset);
            }
        }
    }
}
