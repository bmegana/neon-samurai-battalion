﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;

    private BoxCollider2D boxCol2d;

    public enum Action
    {
        Attack, Block
    }

    public enum Stance
    {
        High, Low
    }

    public Action action = Action.Attack;
    public Stance stance = Stance.High;

    private bool isBlocked = false;
    private bool playerHit = false;

    private void Awake()
    {
        boxCol2d = GetComponent<BoxCollider2D>();
    }

    private void CheckIfDead(Collider2D col)
    {
        bool uppercut = stance == Stance.High && col.CompareTag("LowAttack");
        bool overhead = stance == Stance.Low && col.CompareTag("HighAttack");
        bool isDead = uppercut || overhead;

        if (isDead)
        {
            SoundPlayer.instance.PlaySoundFX(SoundPlayer.SoundEffect.Slash);
            Destroy(gameObject);
        }
    }

    private void CheckIfBlocked(Collider2D col)
    {
        bool highBlock = stance == Stance.High && col.CompareTag("HighBlock");
        bool lowBlock = stance == Stance.Low && col.CompareTag("LowBlock");
        isBlocked = highBlock || lowBlock;

        if (isBlocked)
        {
            SoundPlayer.instance.PlaySoundFX(SoundPlayer.SoundEffect.Block);
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (action == Action.Block)
        {
            CheckIfDead(col);
        }
        else if (!isBlocked)
        {
            CheckIfBlocked(col);
        }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (action == Action.Block)
        {
            CheckIfDead(col);
        }
        else if (!isBlocked && !playerHit)
        {
            CheckIfBlocked(col);

            Bounds enemyBounds = boxCol2d.bounds;
            float enemyLeftBound = enemyBounds.center.x - enemyBounds.extents.x;
            float playerLeftBound = col.bounds.center.x - col.bounds.extents.x;

            if (enemyLeftBound < playerLeftBound)
            {
                playerHit = true;
                PlayerHealth.instance.DecrementLives();
            }
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
