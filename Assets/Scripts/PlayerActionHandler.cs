﻿using UnityEngine;

public class PlayerActionHandler : MonoBehaviour
{
    public float requiredSliceSpeed;

    private Vector3 standbyPosition = new Vector3(-13, -4, 0);
    private Vector3 actingPosition = new Vector3(-10, -4, 0);
    private bool playerHasAttacked = false;

    public Sprite standby;
    public Sprite uppercut;
    public Sprite overhead;
    public Sprite highBlock;
    public Sprite lowBlock;
    private SpriteRenderer spriteRenderer;

    public GameObject highAttackBox;
    public GameObject lowAttackBox;
    public GameObject highBlockBox;
    public GameObject lowBlockBox;

    public float maxTouchTime;
    private float currTouchTime;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        currTouchTime = maxTouchTime;
    }

    private void HandleTouchInput()
    {
        if (Input.touchCount > 0 && currTouchTime > 0.0f)
        {
            currTouchTime -= Time.deltaTime;

            Touch touch = Input.GetTouch(0);
            if (Mathf.Abs(touch.deltaPosition.y) < requiredSliceSpeed)
            {
                if (touch.position.y > Screen.height / 2.0f)
                {
                    spriteRenderer.sprite = highBlock;
                    transform.position = actingPosition;
                    highBlockBox.SetActive(true);
                }
                else
                {
                    spriteRenderer.sprite = lowBlock;
                    transform.position = actingPosition;
                    lowBlockBox.SetActive(true);
                }
            }
            if (touch.deltaPosition.y < -requiredSliceSpeed)
            {
                playerHasAttacked = true;
                spriteRenderer.sprite = overhead;
                transform.position = actingPosition;
                highAttackBox.SetActive(true);
            }
            else if (touch.deltaPosition.y > requiredSliceSpeed)
            {
                playerHasAttacked = true;
                spriteRenderer.sprite = uppercut;
                transform.position = actingPosition;
                lowAttackBox.SetActive(true);
            }
        }
        else
        {
            if (!playerHasAttacked)
            {
                spriteRenderer.sprite = standby;
                transform.position = standbyPosition;
            }

            highAttackBox.SetActive(false);
            lowAttackBox.SetActive(false);
            highBlockBox.SetActive(false);
            lowBlockBox.SetActive(false);
        }

        if (playerHasAttacked && currTouchTime <= 0.0f)
        {
            spriteRenderer.sprite = standby;
            transform.position = standbyPosition;
        }

        if (Input.touchCount == 0)
        {
            playerHasAttacked = false;
            currTouchTime = maxTouchTime;
        }
    }

    private void HandleMouseInput()
    {
        if ((Input.GetMouseButtonDown(0) || Input.GetMouseButton(0)) &&
            currTouchTime > 0.0f && !playerHasAttacked)
        {
            currTouchTime -= Time.deltaTime;

            float vertMouseSpeed = Input.GetAxis("Mouse Y");
            if (Mathf.Abs(vertMouseSpeed) < requiredSliceSpeed)
            {
                if (Input.mousePosition.y > Screen.height / 2.0f)
                {
                    spriteRenderer.sprite = highBlock;
                    transform.position = actingPosition;
                    highBlockBox.SetActive(true);
                }
                else
                {
                    spriteRenderer.sprite = lowBlock;
                    transform.position = actingPosition;
                    lowBlockBox.SetActive(true);
                }
            }
            else if (vertMouseSpeed > requiredSliceSpeed)
            {
                playerHasAttacked = true;
                spriteRenderer.sprite = uppercut;
                transform.position = actingPosition;
                lowAttackBox.SetActive(true);
            }
            else if (vertMouseSpeed < -requiredSliceSpeed)
            {
                playerHasAttacked = true;
                spriteRenderer.sprite = overhead;
                transform.position = actingPosition;
                highAttackBox.SetActive(true);
            }
        }
        else
        {
            if (!playerHasAttacked)
            {
                spriteRenderer.sprite = standby;
                transform.position = standbyPosition;
            }

            highAttackBox.SetActive(false);
            lowAttackBox.SetActive(false);
            highBlockBox.SetActive(false);
            lowBlockBox.SetActive(false);
        }

        if (playerHasAttacked && currTouchTime <= 0.0f)
        {
            spriteRenderer.sprite = standby;
            transform.position = standbyPosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            playerHasAttacked = false;
            currTouchTime = maxTouchTime;
        }
    }

    private void Update()
    {
        HandleTouchInput();
        //HandleMouseInput();
    }
}
