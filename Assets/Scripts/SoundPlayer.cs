﻿using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public static SoundPlayer instance;

    [SerializeField] private SoundFXClips clips = null;
    private AudioSource audioSource;

    public enum SoundEffect
    {
        Slash, Block
    }

    private void Awake()
    {
        if (instance != this)
        {
            instance = this;
            audioSource = GetComponent<AudioSource>();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void PlaySoundFX(SoundEffect sound)
    {
        switch (sound)
        {
            case SoundEffect.Slash:
                audioSource.PlayOneShot(clips.slash);
                break;
            case SoundEffect.Block:
                audioSource.PlayOneShot(clips.block);
                break;
        }
    }
}
