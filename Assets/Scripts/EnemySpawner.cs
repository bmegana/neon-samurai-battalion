﻿using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public float enemySpeed;
    public GameObject highAttack;
    public GameObject lowAttack;
    public GameObject highBlock;
    public GameObject lowBlock;

    [SerializeField] private Transform clashArea = null;
    [SerializeField] private Transform spawnArea = null;
    private float neededNoteTravelTime;

    [SerializeField] private BeatMap beatMap = null;
    private List<float> beatPositions;
    private int nextBeatIndex;

    [SerializeField] private Conductor conductor = null;

    private void Start()
    {
        float noteTravelDistance = Vector2.Distance(
            spawnArea.position, clashArea.position
        );
        neededNoteTravelTime = noteTravelDistance / enemySpeed;

        beatPositions = beatMap.GetBeatPositions();
        nextBeatIndex = 0;
    }

    private void Update()
    {
        if (nextBeatIndex < beatPositions.Count)
        {
            float songPos = conductor.GetSongPosition();
            float nextBeatInSec = beatPositions[nextBeatIndex];
            if (songPos + neededNoteTravelTime >= nextBeatInSec)
            {
                nextBeatIndex++;
                bool stanceIsHigh = Random.Range(0, 2) == 0;
                bool isAttacking = Random.Range(0, 2) == 0;

                GameObject enemy;
                if (stanceIsHigh)
                {
                    if (isAttacking)
                    {
                        enemy = Instantiate(highAttack);
                    }
                    else
                    {
                        enemy = Instantiate(highBlock);
                    }
                }
                else
                {
                    if (isAttacking)
                    {
                        enemy = Instantiate(lowAttack);
                    }
                    else
                    {
                        enemy = Instantiate(lowBlock);
                    }
                }

                if (enemy != null)
                {
                    Rigidbody2D enemyRb2d = enemy.GetComponent<Rigidbody2D>();
                    enemyRb2d.velocity = Vector2.left * enemySpeed;
                }
            }
        }
    }
}
