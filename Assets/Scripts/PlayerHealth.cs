﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public static PlayerHealth instance;

    [SerializeField] private int numLives = 10;
    [SerializeField] private Text numLivesText = null;

    private void KillPlayer()
    {
        Destroy(gameObject);
        LevelController.instance.StopLevel();
    }

    public void DecrementLives()
    {
        numLives--;
        numLivesText.text = "x" + numLives.ToString();

        if (numLives <= 0)
        {
            KillPlayer();
        }
    }

    private void Awake()
    {
        if (instance != this)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        numLivesText.text = "x" + numLives.ToString();
    }
}
