﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public static LevelController instance;

    [SerializeField] private Text resetLevelPrompt = null;
    private bool levelStopped = false;

    public void StopLevel()
    {
        Time.timeScale = 0.0f;
        resetLevelPrompt.text = "Tap Anywhere to Reset";
        levelStopped = true;
    }

    private void Awake()
    {
        if (instance != this)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void ResetLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1.0f;
        resetLevelPrompt.text = "";
        levelStopped = false;
    }

    private void Update()
    {
        if (levelStopped && Input.anyKeyDown)
        {
            ResetLevel();
        }
    }
}
