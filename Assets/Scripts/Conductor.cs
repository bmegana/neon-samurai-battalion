﻿using UnityEngine;

public class Conductor : MonoBehaviour
{
    [SerializeField] private BeatMap beatMap = null;
    
    private float dspTimeSong;
    private float songPosition = 0.0f;

    private AudioSource audioSource;

    public float GetSongPosition()
    {
        return songPosition;
    }

    private void Awake()
    {
        dspTimeSong = (float)AudioSettings.dspTime;

        audioSource = GetComponent<AudioSource>();
        audioSource.clip = beatMap.GetSong();
        audioSource.Play();
    }

    private void OnAudioFilterRead(float[] data, int channels)
    {
        songPosition = (float)(AudioSettings.dspTime - dspTimeSong) -
            beatMap.GetOffset();
    }
}
